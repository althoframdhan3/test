![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Example [GitBook] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---



Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
